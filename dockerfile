FROM node:latest

ADD . ./

RUN npm install

EXPOSE 3000
VOLUME /app/logs

CMD node app.js